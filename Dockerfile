FROM pusoremih/builder:latest

COPY . /

RUN npm install

ARG TOKEN
ARG USER

ENV TOKEN $TOKEN
ENV USER $USER

RUN sh /bin/ci.sh
